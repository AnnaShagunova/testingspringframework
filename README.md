# TestingSpringFramework



## Spring Core learning

1.	Первое приложение (IntelliJ Idea).

2.	Инверсия управления. Inversion of Control (IoC).

3.	Внедрение зависимостей. Dependency Injection (DI). Введение.

4.	Внедрение зависимостей. Dependency Injection (DI). Часть 2.

5.	Bean scope (Область видимости бинов).

6.	Жизненный цикл бина (Bean Lifecycle). Init, Destroy и Factory методы.

7.	Аннотации. Введение.

8.  Аннотация @Autowired. Внедрение зависимостей (Dependency Injection).

9.	Аннотация @Qualifier. Внедрение зависимостей (Dependency Injection).

10.	Аннотации @Scope, @Value, @PostConstruct, @PreDestroy.

11.	Конфигурация с помощью Java кода.
