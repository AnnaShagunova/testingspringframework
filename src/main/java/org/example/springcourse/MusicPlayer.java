package org.example.springcourse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MusicPlayer {

    @Value("${musicPlayer.name}")
    private String name;
    @Value("${musicPlayer.volume}")
    private int volume;

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    private Music musicRock;
    private Music musicPop;

    @Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music musicRock,
                       @Qualifier("popMusic") Music musicPop) {
        this.musicRock = musicRock;
        this.musicPop = musicPop;
    }

    public String playMusic() {
        return "Playing: " + musicRock.getSong() + ", " + musicPop.getSong();
    }
}
