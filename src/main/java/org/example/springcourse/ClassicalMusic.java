package org.example.springcourse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("classicalMusic")
@Scope("prototype")
public class ClassicalMusic implements Music{
    @Override
    public String getSong() {
        return "J.S. Bach - Wir Christenleut', BWV 612";
    }
}
